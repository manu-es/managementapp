package app.management.generateEntities;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import app.management.entity.Ccaa;
import app.management.repository.CCAARepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class GenerateDirectoryEntry {
	
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private CCAARepository ccaaRepository;
	
	@Test
	public void test() {
		/*int resultadoReal = 1;
	    int resultadoEsperado = 1;
	    assertEquals(resultadoEsperado, resultadoReal);*/
		
		/*this.entityManager.persist(new User("sboot", "1234"));
		User user = this.repository.findByUsername("sboot");
		assertThat(user.getUsername()).isEqualTo("sboot");
		assertThat(user.getVin()).isEqualTo("1234");*/
		
		Ccaa ccaa = new Ccaa();
		ccaa.setName("a");
		this.entityManager.persist(ccaa);
		
		List<Ccaa> ccaaList = ccaaRepository.findAll();
		System.out.println("numero de ccaa "+ ccaaList.size());
		
		//Ccaa ccaa = ccaaRepository.findByName("Cantabria");
		//System.out.println("numero de id "+ ccaa.getId());
	}

}
