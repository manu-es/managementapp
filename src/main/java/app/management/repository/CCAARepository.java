package app.management.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.management.entity.Ccaa;

@Repository("ccaaRepository")
public interface CCAARepository extends JpaRepository<Ccaa, Serializable>{
	
	public Ccaa findByName(String name);

}
