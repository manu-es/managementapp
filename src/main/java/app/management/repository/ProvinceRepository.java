package app.management.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.management.entity.Province;

@Repository("provinceRepository")
public interface ProvinceRepository extends JpaRepository<Province, Serializable>{

}
