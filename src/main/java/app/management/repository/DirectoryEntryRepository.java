package app.management.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.management.entity.DirectoryEntry;

@Repository("directoryEntryRepository")
public interface DirectoryEntryRepository extends JpaRepository<DirectoryEntry, Serializable>{

}
