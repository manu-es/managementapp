package app.management.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the directoryentry database table.
 * 
 */
@Entity
@Table(name = "directoryentry")
@NamedQuery(name="DirectoryEntry.findAll", query="SELECT d FROM DirectoryEntry d")
public class DirectoryEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private byte active;

	private String address;

	@Temporal(TemporalType.DATE)
	private Date birthdate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdate", insertable=false)
	private Date createdate;

	private String dni;

	private String email;

	private int gender;

	private String locality;

	private String name;

	private String postalcode;

	private String surname1;

	private String surname2;

	private String telephone;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedate;

	//bi-directional many-to-one association to Province
	@ManyToOne
	@JoinColumn(name="province")
	private Province provinceBean;

	public DirectoryEntry() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthdate() {
		return this.birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Date getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getGender() {
		return this.gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public String getLocality() {
		return this.locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPostalcode() {
		return this.postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getSurname1() {
		return this.surname1;
	}

	public void setSurname1(String surname1) {
		this.surname1 = surname1;
	}

	public String getSurname2() {
		return this.surname2;
	}

	public void setSurname2(String surname2) {
		this.surname2 = surname2;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Date getUpdatedate() {
		return this.updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public Province getProvinceBean() {
		return this.provinceBean;
	}

	public void setProvinceBean(Province provinceBean) {
		this.provinceBean = provinceBean;
	}

}