package app.management.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the province database table.
 * 
 */
@Entity
@NamedQuery(name="Province.findAll", query="SELECT p FROM Province p")
public class Province implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String name;

	//bi-directional many-to-one association to Directoryentry
	@OneToMany(mappedBy="provinceBean", fetch=FetchType.EAGER)
	private List<DirectoryEntry> directoryentries;

	//bi-directional many-to-one association to Ccaa
	@ManyToOne
	@JoinColumn(name="ccaa")
	private Ccaa ccaaBean;

	public Province() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DirectoryEntry> getDirectoryentries() {
		return this.directoryentries;
	}

	public void setDirectoryentries(List<DirectoryEntry> directoryentries) {
		this.directoryentries = directoryentries;
	}

	public DirectoryEntry addDirectoryentry(DirectoryEntry directoryentry) {
		getDirectoryentries().add(directoryentry);
		directoryentry.setProvinceBean(this);

		return directoryentry;
	}

	public DirectoryEntry removeDirectoryentry(DirectoryEntry directoryentry) {
		getDirectoryentries().remove(directoryentry);
		directoryentry.setProvinceBean(null);

		return directoryentry;
	}

	public Ccaa getCcaaBean() {
		return this.ccaaBean;
	}

	public void setCcaaBean(Ccaa ccaaBean) {
		this.ccaaBean = ccaaBean;
	}

}