package app.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import app.management.entity.Ccaa;
import app.management.entity.DirectoryEntry;
import app.management.repository.CCAARepository;
import app.management.repository.DirectoryEntryRepository;
import app.management.repository.ProvinceRepository;

@Controller
@RequestMapping("/directoryentry")
public class DirectoryEntryController {
	
	@Autowired
	@Qualifier("ccaaRepository")
	private CCAARepository ccaaRepository;
	
	@Autowired
	@Qualifier("directoryEntryRepository")
	private DirectoryEntryRepository directoryEntryRepository;
	
	@Autowired
	@Qualifier("provinceRepository")
	private ProvinceRepository provinceRepository;
	
	@GetMapping("/list")
	public String list(){
		List<Ccaa> ccaaList = ccaaRepository.findAll();
		System.out.println("numero de ccaa "+ ccaaList.size());
		
		Ccaa ccaa = ccaaRepository.findByName("Cantabria");
		System.out.println("numero de id "+ ccaa.getId());
		return "list";
	}
	
	@GetMapping("/form-directoryentry")
	public String redirectFormContacto(@RequestParam(name="id", required=false, defaultValue = "0") int id, // para la edición del DirectoryEntry
			Model model){ // para añadir el DirectoryEntry
		DirectoryEntry directoryEntry = new DirectoryEntry();
		if(id != 0){
			//directoryEntry = contactoServicio.findContactoModelById(id); //Añadimos el bean con datos al formulario para editar
		}
		//model.addAttribute("contactoModel", contactoModel); //Añadimos el bean vacío al formulario para añadir
		model.addAttribute("directoryEntry", directoryEntry);
		model.addAttribute("ccaaList", ccaaRepository.findAll());
		model.addAttribute("provinceList", provinceRepository.findAll());
		return "form-directoryentry";
	}
	
	
	@PostMapping("/add-directoryentry")
	public String addDirectoryentry(@ModelAttribute(name="directoryEntry") DirectoryEntry directoryEntry, Model model){ // directoryEntry ->objeto que recibe del formulario
		if(null != directoryEntryRepository.save(directoryEntry)){
			model.addAttribute("resultado", 1);
		} else {
			model.addAttribute("resultado", 0);
		}
		return "form-directoryentry";
	}

}
